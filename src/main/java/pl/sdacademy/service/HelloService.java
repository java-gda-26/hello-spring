package pl.sdacademy.service;

import org.springframework.stereotype.Component;
import pl.sdacademy.repository.GreetingRepository;

@Component
public class HelloService {

    private GreetingsService greetingsDbService;
    private GreetingsService greetingsMemoryService;
    private GreetingsService greetingsClasspathService;

    public HelloService(
            GreetingsService greetingsDbService,
            GreetingsService greetingsMemoryService,
            GreetingsService greetingsClasspathService) {
        this.greetingsDbService = greetingsDbService;
        this.greetingsMemoryService = greetingsMemoryService;
        this.greetingsClasspathService = greetingsClasspathService;
    }

    public void sayHello(String type) {
        String greeting;
        switch (type) {
            case "db":
                greeting = greetingsDbService.loadGreetings();
                break;
            case "memory":
                greeting = greetingsMemoryService.loadGreetings();
                break;
            case "classpath":
                greeting = greetingsClasspathService.loadGreetings();
                break;
            default:
                throw new RuntimeException("Unknown type");
        }

        System.out.println(greeting);
    }
}
