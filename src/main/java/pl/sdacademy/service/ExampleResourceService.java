package pl.sdacademy.service;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Component
public class ExampleResourceService {

    private ResourceLoader resourceLoader;

    public ExampleResourceService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    // :)
    public String downloadInternet() {
        try {
            Resource resource = resourceLoader.getResource("https://google.com");
            InputStream is = resource.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            return reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException("Error when loading resources", e);
        }
    }
}
