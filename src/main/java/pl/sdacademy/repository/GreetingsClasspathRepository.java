package pl.sdacademy.repository;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Component
public class GreetingsClasspathRepository implements GreetingRepository {

    @Value("${classpath:file.txt}")
    private Resource classpathResource;

    @Override
    public String getGreeting() {
        try {
            InputStream is = classpathResource.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String output = reader.readLine();
            is.close();
            reader.close();
            return output;
        } catch (IOException e) {
            throw new RuntimeException("Error when loading resources", e);
        }
    }
}
