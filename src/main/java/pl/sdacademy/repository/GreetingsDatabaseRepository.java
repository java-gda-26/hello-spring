package pl.sdacademy.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("dbRepository")
public class GreetingsDatabaseRepository implements GreetingRepository {

    @Value("${db.url}")
    private String jdbcUrl;

    public String getGreeting() {
        return "Greeting from db: " + jdbcUrl;
    }
}
