package pl.sdacademy.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("pl.sdacademy")
@PropertySource("application.properties")
public class ApplicationConfig {
}
